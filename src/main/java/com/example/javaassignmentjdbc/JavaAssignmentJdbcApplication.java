package com.example.javaassignmentjdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaAssignmentJdbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaAssignmentJdbcApplication.class, args);
    }

}
