package com.example.javaassignmentjdbc.models;


public record CustomerSpender(int customer_id,
                              String first_name,
                              String last_name,
                              double total) {

}

