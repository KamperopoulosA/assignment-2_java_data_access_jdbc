package com.example.javaassignmentjdbc.models;


public record CustomerCountry(String country,int count) {
}
