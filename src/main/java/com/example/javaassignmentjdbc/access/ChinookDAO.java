package com.example.javaassignmentjdbc.access;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
public class ChinookDAO {

    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;;

    public ChinookDAO(){

    }

    public void testConnection(){// test connection to database
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            System.out.println("Connected");


        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }
}
