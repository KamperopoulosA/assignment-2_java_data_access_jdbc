package com.example.javaassignmentjdbc.runners;

import com.example.javaassignmentjdbc.access.ChinookDAO;
import com.example.javaassignmentjdbc.models.Customer;
import com.example.javaassignmentjdbc.repositories.CustomerRepository;
import com.example.javaassignmentjdbc.repositories.CustomerRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.sql.SQLOutput;

@Component
public class AppRunner implements ApplicationRunner {//runner class
    private final CustomerRepository customerRepository;
@Autowired
    public AppRunner(CustomerRepository customerRepository) {//constructor injection
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {//calls functions

        System.out.println(customerRepository.findAll());

        System.out.println(customerRepository.findById(5));

        System.out.println(customerRepository.getByName("Daan","Peeters"));
        System.out.println(customerRepository.getCustomersPage(3,5));
        System.out.println(customerRepository.insert(new Customer(201,"George","Kamperopoulos","Greece","30300","+306999999999","f.staikos7@gmail.com")));
        System.out.println(customerRepository.update(100,"Fotis","Staikos","Greece","44300","+306999999999","f.staikos7@gmail.com"));
        System.out.println(customerRepository.getCountryWithMostCustomers());
        System.out.println(customerRepository.getHighestSpender());
        System.out.println(customerRepository.getMostPopularGenre(20));
    }
}


