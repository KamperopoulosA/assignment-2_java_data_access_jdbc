package com.example.javaassignmentjdbc.repositories;

import java.util.List;
a generic repository to encapsulate CRUD
/*CRUDRepository interface is a generic repository to encapsulate CRUD */
/*   T: Domain type that repository manages (Generally the Entity/Model class name)
    ID: Type of the id of the entity that repository manages (Generally the wrapper class of your @Id that is created inside the Entity/Model class) */

public interface CRUDRepository<ID, T> {
   
   /*findAll: Returns all instances of the type.
   Return Type: All entities */
   
    List<T> findAll();
    
    /*    findbyId: Retrieves an entity by its id.
    Parameters: id – must not be null.
    Returns: the entity with the given id or Optional#empty() if none found.
    Exception Thrown: IllegalArgumentException is thrown if the ‘id’ is null. */
    T findById(ID id);
    
    
    /*insert: insert a new record to the database return rows affected.
      Parameter: Domain type object that repository manages */
    int insert(T object);

     /*update: update a  record from the database return rows affected.
      Parameters: column values to be updated */
    int update(int id, String firstName, String lastName, String country, String postalCode, String phoneNumber, String email);

}
