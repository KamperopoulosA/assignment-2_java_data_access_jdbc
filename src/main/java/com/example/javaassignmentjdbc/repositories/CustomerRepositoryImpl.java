package com.example.javaassignmentjdbc.repositories;

import com.example.javaassignmentjdbc.models.Customer;
import com.example.javaassignmentjdbc.models.CustomerCountry;
import com.example.javaassignmentjdbc.models.CustomerGenre;
import com.example.javaassignmentjdbc.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository{

    private final String url;
    private final String username;
    private final String password;

    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /*1. Read all the customers in the database, this should display their: Id, first name, last name, country, postal code,
    phone number and email */
    @Override
    public List<Customer> findAll() {
    String sql ="SELECT * FROM Customer";
    List<Customer> customers = new ArrayList<>();
    try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
        while(result.next()) {
            Customer customer= new Customer(
                    result.getInt("customer_id"),
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("country"),
                    result.getString("postal_code"),
                    result.getString("phone"),
                    result.getString("email")

                );
             customers.add(customer);

            }
        } catch (SQLException e) {
         e.printStackTrace();
        }
        return customers;
    }

    /*2. Read a specific customer from the database (by Id), should display everything listed in the above point*/

    @Override
    public Customer findById(Integer id) {
       String sql = "SELECT * FROM customer WHERE customer_id =(?)";
        Customer customer = null;
       try( Connection conn =  DriverManager.getConnection(url, username,password)){
           // Write statement
           PreparedStatement statement = conn.prepareStatement(sql);
           statement.setInt(1,id);
           // Execute statement
           ResultSet result = statement.executeQuery();
           // Handle result
           while(result.next()) {
               customer = new Customer(
                       result.getInt("customer_id"),
                       result.getString("first_name"),
                       result.getString("last_name"),
                       result.getString("country"),
                       result.getString("postal_code"),
                       result.getString("phone"),
                       result.getString("email")
               );

           }

       }catch(SQLException e){
        e.printStackTrace();
       }
        return customer;
    }

        /*3. Read a specific customer by name.*/
    @Override
    public Customer getByName(String firstName, String lastName) {

        String sql = "SELECT * FROM customer WHERE first_name LIKE ? AND last_name LIKE ?";
        Customer customer = null;
        try (Connection connection = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, "%" + firstName + "%");
            statement.setString(2, "%" + lastName + "%");
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                 customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );

            }


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return customer;
    }

    /* 4. Return a page of customers from the database. This should take in limit and offset as parameters and make use
    of the SQL limit and offset keywords to get a subset of the customer data.    */
    @Override
    public List<Customer> getCustomersPage(int limit,int offset) {
        String sql = "SELECT * FROM customer LIMIT ? OFFSET ?";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,limit);
            statement.setInt(2,offset);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }


        }catch(SQLException e){
            e.printStackTrace();
        }
        return customers;
    }

    /*5. Add a new customer to the database.  */

    @Override
    public int insert(Customer customer){
        String sql = "INSERT INTO customer(customer_id,first_name,last_name,country,postal_code,phone,email)VALUES(?,?,?,?,?,?,?)";
        int updateResult = 0;
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,customer.customer_id());
            statement.setString(2, customer.first_name());
            statement.setString(3, customer.last_name());
            statement.setString(4, customer.country());
            statement.setString(5, customer.postal_code());
            statement.setString(6, customer.phone());
            statement.setString(7, customer.email());
            updateResult = statement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return updateResult;

    }

    /*6. Update an existing customer.*/
    @Override

    public int update(int id, String firstName, String lastName, String country, String postalCode, String phoneNumber, String email) {
        int updateResult = 0;
        try (Connection connection = DriverManager.getConnection(url,username,password)) {
            String sql = "UPDATE customer SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? WHERE customer_id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setString(3, country);
            statement.setString(4, postalCode);
            statement.setString(5, phoneNumber);
            statement.setString(6, email);
            statement.setInt(7, id);
            updateResult =statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return updateResult;
    }

    /*7. Return the country with the most customers.*/
   @Override
    public CustomerCountry getCountryWithMostCustomers() {
       String sql = "SELECT country, COUNT(*) as count FROM customer GROUP BY country ORDER BY count ASC";
       CustomerCountry country =null;
        try (Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                country = new CustomerCountry(resultSet.getString("country"), resultSet.getInt("count"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
       return country;
    }

    /*8. Customer who is the highest spender (total in invoice table is the largest).*/

    @Override
    public CustomerSpender getHighestSpender(){
        String sql = "SELECT invoice.customer_id,customer.first_name,customer.last_name,invoice.total FROM invoice INNER JOIN customer ON invoice.customer_id = customer.customer_id" +
                " WHERE total = (SELECT MAX(total) from invoice)";
        CustomerSpender spender = null;
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                spender = new CustomerSpender(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getDouble("total")
                );
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return spender;
    }
    /*9. For a given customer, their most popular genre (in the case of a tie, display both)*/
    @Override
    public List<CustomerGenre> getMostPopularGenre(int customerId) {
        String query = "SELECT genre.name as genre, COUNT(*) as count " +
                "FROM customer " +
                "JOIN invoice ON customer.customer_id = invoice.customer_id " +
                "JOIN invoice_line ON invoice.invoice_id = invoice_line.invoice_id " +
                "JOIN track ON invoice_line.track_id = track.track_id " +
                "JOIN genre ON track.genre_id = genre.genre_id " +
                "WHERE customer.customer_id = ? " +
                "GROUP BY genre.name " +
                "ORDER BY count DESC ";
        List<CustomerGenre> genres = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, customerId);
            ResultSet resultSet = statement.executeQuery();

            int max = 0;
            while(resultSet.next()) {
                if (resultSet.getInt("count") >= max) {
                    max = resultSet.getInt("count");
                    genres.add(new CustomerGenre(resultSet.getString("genre"), resultSet.getInt("count")));
                }
            }


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return genres;
    }








}


