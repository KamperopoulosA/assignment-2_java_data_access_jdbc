package com.example.javaassignmentjdbc.repositories;

import com.example.javaassignmentjdbc.models.Customer;
import com.example.javaassignmentjdbc.models.CustomerCountry;
import com.example.javaassignmentjdbc.models.CustomerGenre;
import com.example.javaassignmentjdbc.models.CustomerSpender;
import org.yaml.snakeyaml.events.Event;

import java.util.List;

public interface CustomerRepository extends CRUDRepository<Integer, Customer> {

    /*getByName: Retrieves an customer by its name
      Parameters: customer's first name and last name to search for 
      return: return specific customer by its name*/

    Customer getByName(String firstName, String lastName);


    List<Customer> getCustomersPage(int limit,int offset);

    /*getCountryWithMostCustomers: Retrieves country with most customers
      return CustomerCountry type object that is the country with most customers */
    CustomerCountry getCountryWithMostCustomers();

    /*getHighestSpender: Retrieves customer who is the highest spender
      return CustomerSpender type object that is the highest spender */
    CustomerSpender getHighestSpender();

    /*getMostPopoularGenre: Retrieves most popular genre for a specific customer
      Parameter: customer's id to search for
      return: CustomerGenre type List that contains the most popular genre*/
    List<CustomerGenre> getMostPopularGenre(int customerId);
}
