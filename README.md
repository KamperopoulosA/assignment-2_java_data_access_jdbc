# Assignment 2_Java_Data_Access_JDBC

### Appendix A
Several SQL scripts which can be run to create a database, setup some tables in the database, add
relationships to the tables, and then populate the tables with data.

### Appendix B

This is a Spring Boot application that provides the following functionality for managing customers in a database:

- Read all the customers in the database, this should display their: Id, first name, last name, country, postal code, phone number and email.
- Read a specific customer from the database (by Id), should display everything listed in the above point.
- Read a specific customer by name. Using his/her first and last name.
- Return a page of customers from the database. This should take in limit and offset as parameters and make use of the SQL limit and offset keywords to get a subset of the customer data. The customer model from above should be reused.
- Add a new customer to the database. You also need to add only the fields listed above (our customer object)
- Update an existing customer.
- Return the country with the most customers.
- Customer who is the highest spender.
- For a given customer, their most popular genre (in the case of a tie, display both). Most popular in this context means the genre that corresponds to the most tracks from invoices associated to that customer.

## Prerequisites

- Java 8 or higher
- Gradle
- PostgreSQL

## Contributing

- [Fotis Staikos](https://gitlab.com/f.staikos7)
- [Kamperopoulos Anastasios](https://gitlab.com/KamperopoulosA)

## Getting started

1. Clone the repository and navigate to the root directory of the project
`git clone https://gitlab.com/KamperopoulosA/assignment-2_java_data_access_jdbc.git
cd assignment-2_java_data_access_jdbc
`
2. Create a database and update the application.properties file with the appropriate database connection details.

3. Build and run the application using the following command:
`gradle clean build
gradle bootRun`

## Built with

- Spring boot
- PostgreSQL (for database storage)
- Gradle

