-- Database: SuperheroesDb

-- DROP DATABASE IF EXISTS "SuperheroesDb";

DROP TABLE IF EXISTS Superhero;
DROP TABLE IF EXISTS Assistant;
DROP TABLE IF EXISTS Power;

CREATE TABLE Superhero (
    shero_id SERIAL PRIMARY KEY,
    shero_name varchar(50) NOT NULL,
    shero_alias varchar(50),
	shero_origin varchar(250)
);

CREATE TABLE Assistant (
    assis_id SERIAL PRIMARY KEY,
    assis_name varchar(50) NOT NULL
    
);

CREATE TABLE Power (
    power_id SERIAL PRIMARY KEY,
    power_name varchar(50) NOT NULL,
    power_des varchar(250)
	
);