-- Database: SuperheroesDb

-- DROP DATABASE IF EXISTS "SuperheroesDb";

ALTER TABLE Assistant
ADD CONSTRAINT fk_superhero
FOREIGN KEY (assis_id)
REFERENCES Superhero (shero_id)
ON DELETE CASCADE;