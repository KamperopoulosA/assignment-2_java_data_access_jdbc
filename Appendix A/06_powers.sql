-- Database: SuperheroesDb

-- DROP DATABASE IF EXISTS "SuperheroesDb";

INSERT INTO Power
    (power_id,power_name, power_des)
VALUES
    (1, 'Super Strength', 'The ability to have strength beyond normal human limits'),
    (2, 'Super Speed', 'The ability to move and react at superhuman speeds'),
    (3, 'Super Durability', 'The ability to withstand damage and injury greater than that of a normal human'),
    (4, 'Super Intelligence', 'The ability to have intelligence beyond that of a normal human');

INSERT INTO SuperheroPower
    (SuperheroId, PowerId)
VALUES
    (1, 1),
    (1, 2),
    (1, 3),
    (1, 4),
    (2, 1),
    (3, 2)
ON CONFLICT DO NOTHING;