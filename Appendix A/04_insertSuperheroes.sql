-- Database: SuperheroesDb

-- DROP DATABASE IF EXISTS "SuperheroesDb";

INSERT INTO Superhero
    (shero_id,shero_name, shero_alias, shero_origin)
VALUES
    (1, 'Superman', 'Clark Kent', 'Krypton');

INSERT INTO Superhero
    (shero_id,shero_name, shero_alias, shero_origin)
VALUES
    (2, 'Wonder Woman', 'Diana Prince', 'Themyscira');

INSERT INTO Superhero
    (shero_id,shero_name, shero_alias, shero_origin)
VALUES
    (3, 'Spider-Man', 'Peter Parker', 'New York City');