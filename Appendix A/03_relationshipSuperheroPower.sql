-- Database: SuperheroesDb

-- DROP DATABASE IF EXISTS "SuperheroesDb";
DROP TABLE IF EXISTS SuperheroPower;

CREATE TABLE SuperheroPower (
    SuperheroId INTEGER REFERENCES Superhero(shero_id),
    PowerId INTEGER REFERENCES Power(power_id),
    PRIMARY KEY (SuperheroId, PowerId)
);

ALTER TABLE SuperheroPower
ADD CONSTRAINT fk_superhero
FOREIGN KEY (SuperheroId)
REFERENCES Superhero (shero_id)
ON DELETE CASCADE;

ALTER TABLE SuperheroPower
ADD CONSTRAINT fk_power
FOREIGN KEY (PowerId)
REFERENCES Power (power_id)
ON DELETE CASCADE;

